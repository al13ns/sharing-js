
var cmp = Vue.extend(
{
    route    :  {
        canReuse : function(trans)
        {
            return false;
        }
    },
    data        :   function()
    {
        return {
            shares  :   [] ,
            cutTo   :   30
        };
    },
    methods     :
    {
        cutString   :   function( string , places )
        {
            if( string.length <= places )
                return string;

            return string.substring( 0 , places - 3 ) + '...';
        },

        removeShare  :   function( shareId )
        {
            var self = this;

            self.$route.router.app.shareManager().removeItem( shareId )
            .then(function()
            {
                self.$route.router.go({ name : 'shares' , query : { '_' : self.$momentUtime() } });
            });
        }
    },
    ready   :   function()
    {
        var app     =   this.$route.router.app;
        var self    =   this;

        app.shareManager().fetchUserItems( app.loggedUser().uid )
        .then( function( data )
        {
            self.$set( 'shares' , data );
            console.log( data );
        });
    }
});
