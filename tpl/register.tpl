<form class="" @submit.prevent="register">
    <div class="form-group">
        <label for="register-email">Email</label>
        <input v-model="email" type="email" class="form-control" id="register-email" placeholder="Your email" />
    </div>
    <div class="form-group">
        <label for="register-username">Username</label>
        <input v-model="username" type="text" class="form-control" id="register-username" placeholder="Your username" />
    </div>
    <div class="form-group">
        <label for="register-password">Password</label>
        <input v-model="password" type="password" class="form-control" id="register-password" placeholder="Your password" />
    </div>
    <div class="form-group">
        <label for="register-password-repeat">Repeat Password</label>
        <input v-model="password2" type="password" class="form-control" id="register-password-repeat" placeholder="Your password" />
    </div>

    <div class="form-group">
        <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-warning">Register</button>
        </div>
    </div>
</form>