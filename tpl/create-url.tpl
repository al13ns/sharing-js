<form class="" @submit.prevent="createUrl">
    <div class="form-group">
        <label for="create-url-text">Url</label>
        <input v-model="url" type="url" class="form-control" id="create-url-text" placeholder="Url" />
    </div>
    <div class="form-group">
        <label for="create-url-valid-views">Valid for views</label>
        <input v-model="validViews" type="number" class="form-control" id="create-url-valid-views" placeholder="Number of views" />
    </div>
    <div class="form-group">
        <label for="create-url-valid-until">Valid until</label>
        <input v-model="validUntil" type="text" class="form-control" id="create-url-valid-until" placeholder="{{ new Date(now) }}" />
    </div>

    <div class="form-group">
        <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-warning">Create</button>
        </div>
    </div>
</form>