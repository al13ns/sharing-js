
var cmp = Vue.extend(
    {
        data        :   function()
        {
            return {
                item    :   null ,
                url     :   document.URL , //document.location.toString() , document.URL , window.location.toString()
                clpbrd  :   null ,
            };
        },
        methods     :
        {
            isImage :   function( item )
            {
                if( /image/.test( item.metadata.contentType ) )
                    return true;
                else
                    return false;
            },
            downloadText    :   function( item )
            {
                this.downloadTextFile( item.id + '.txt' , item.text );
            },
            downloadTextFile    :   function( filename , text )
            {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
                element.setAttribute('download', filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            },
            clickUrl    :   function()
            {
                this.$els.url.select();
                return true;
            }
        },
        ready   :   function()
        {
            //fetch share, increment views

            var shareId =   this.$route.params.shareId;
            var manager =   this.$route.router.app.shareManager();
            var self    =   this;

            manager.fetchItem( shareId )
            .then(function( item )
            {
                manager.incrementItemViews( shareId );

                console.log(item);

                self.$set( 'item' , item );

                setTimeout(function()
                {
                    console.log( self.$els.url );
                    self.clpbrd =   new Clipboard( self.$els.url );
                    self.clpbrd.on( 'success' ,
                    function(e)
                    {
                        self.$log( 'copied to clipboard' );
                    });
                    self.clpbrd.on( 'error' ,
                    function(e)
                    {
                        self.$log( 'cannot copy to clipboard. press CTRL+C manually.' );
                    });
                }
                , 100 ); //because DOM is not ready immediately after $set
            })
            .catch(function(err)
            {
                self.$log( err , 'danger' );
            });
        },
        beforeDestroy   :   function()
        {
            if( this.clpbrd )
                this.clpbrd.destroy();
        }
    });
