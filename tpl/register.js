
var cmp = Vue.extend(
{
    data: function()
    {
        return {
            username    :   "" ,
            password    :   "" ,
            password2   :   "" ,
            email       :   ""
        };
    },
    methods:
    {
        register    :   function()
        {
            if( !this.email )
            {
                this.$log( 'email is empty' , 'danger' );
                return;
            }
            if( !this.username )
            {
                this.$log( 'username is empty' , 'danger' );
                return;
            }
            if( !this.password )
            {
                this.$log( 'password is empty' , 'danger' );
                return;
            }
            if( this.password !== this.password2 )
            {
                this.$log( 'passwords do not match' , 'danger' );
                return;
            }

            var self = this;

            firebase.auth()
            .createUserWithEmailAndPassword( self.email , self.password )
            .then( function( user )
            {
                console.log( user );

                return user.updateProfile(
                    {
                        displayName : self.username
                    });
            })
            .then( function()
            {
                console.log( firebase.auth().currentUser );
                self.$log( 'successfully registered' , 'success' );

                setTimeout(
                    function()
                    {
                        window.location.assign( 'app.html' );
                        // self.$route.router.go({ name : 'profile' });
                    }, 1000 );
            })
            .catch( function( err )
            {
                self.$log( err.message , 'danger' );
                throw err;
            });
        },

        checkUsername   :   function()
        {
            if( !this.username )
            {
                this.$log( 'username is empty' , 'danger' );
                return;
            }
        }
    },
    ready : function()
    {
        if( this.$route.router.app.loggedUser() )
            this.$route.router.go({ name : 'profile' });
    }
});