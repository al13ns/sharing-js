

var cmp = Vue.extend(
{
    data        :   function()
    {
        var u = this.$route.router.app.loggedUser();

        return {
            user        :   u ,
            name        :   u.displayName ,
            email       :   u.email ,
            photo       :   u.photoURL ,
            passwd      :   '' ,
            newPasswd   :   '' ,
            newPasswd2  :   ''
        };
    },
    methods     :
    {
        updateName : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            this.update(function()
            {
                return user.updateProfile({ 'displayName' : self.name });
            } ,
            'successfully updated name' );
        },
        updatePhoto : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            this.update(function()
            {
                return user.updateProfile({ 'photoURL' : self.photo });
            } ,
            'successfully updated photo' );
        },
        updateEmail : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            this.update(function()
            {
                return user.updateEmail( self.email );
            } ,
            'successfully updated email' );
        },
        updatePassword : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            this.update(function()
            {
                return user.updatePassword( self.newPasswd );
            } ,
            'successfully updated password' );
        },
        update : function( callback , successMessage )
        {
            if( !this.checkPassword() )
                return;

            var self = this;

            return this.reauth()
            .then(function()
            {
                var result = callback.call();

                if( result instanceof Promise )
                    return result;
                else
                    return Promise.resolve( result );
            })
            .then(function()
            {
                self.$log( successMessage , 'success' );
                return self.reload();
            })
            .catch(function(err)
            {
                self.$log( err , 'danger' );
            });
        },
        checkPassword : function()
        {
            if( !this.passwd )
            {
                this.$log( 'enter current password' , 'danger' );
                $( '#profile-password' ).focus();
                return false;
            }
            else
                return true;
        },
        reload : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            return user.reload()
            .then(function()
            {
                return self.reauth();
            });
        },
        reauth : function()
        {
            var user = this.$route.router.app.loggedUser();
            var self = this;

            return new Promise( function( resolve , reject )
            {
                var credential  =   firebase.auth.EmailAuthProvider.credential( user.email , self.passwd );
                console.log( credential );

                user.reauthenticate( credential )
                .then(function()
                {
                    resolve();
                })
                .catch(function(err)
                {
                    self.$log( err , 'danger' );
                    throw err;
                });
            });
        }
    },
    ready : function()
    {

    }
});