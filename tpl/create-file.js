
var cmp = Vue.extend(
{
    data        :   function()
    {
        return {
            filename    :   null ,
            validViews  :   null ,
            validUntil  :   null ,
            now         :   null
        };
    },
    methods     :
    {
        uploadCallback  :   function( uploadTask )
        {
            var self = this;

            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
            function(snapshot)
            {
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                progress    =   Math.round( progress );

                self.$log('Upload is ' + progress + '% done');

                //update progress bar
                $( self.$els.progress )
                    .css( 'width' , progress + '%' )
                    .text( progress + '%' )
                ;

                switch (snapshot.state)
                {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                        self.$log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                        //self.$log('Upload is running');
                        break;
                }
            },
            function(error)
            {
                switch (error.code) {
                    case 'storage/unauthorized':
                        // User doesn't have permission to access the object
                        break;

                    case 'storage/canceled':
                        // User canceled the upload
                        self.$log('Upload was cancelled');
                        break;

                    case 'storage/unknown':
                        // Unknown error occurred, inspect error.serverResponse
                        break;
                }
            },
            function()
            {
                // Upload completed successfully, now we can get the download URL
                var downloadURL = uploadTask.snapshot.downloadURL;

                self.$log( 'Upload finished' );
            });
        },
        createFile  :   function()
        {
            var app     =   this.$route.router.app;
            var self    =   this;
            var file    =   this.$els.file.files[0];

            if( (file.size/1024/1024) > app.maxFileSize )
            {
                self.$log( 'file size exceeded' , 'warning' );
                throw 'file size exceeded'; return;
            }

            app.shareManager()
            .createFile(
                app.loggedUser().uid ,
                file ,
                self.filename || file.name ,
                self.uploadCallback )
            .then(
            function( shareId )
            {
                self.$route.router.go({ name : 'share' , params : { shareId : shareId } });
            });

        },
        selectFile  :   function()
        {
            $( this.$els.file ).click();
        }
    },
    ready   :   function()
    {
        var app     =   this.$route.router.app;
        var self    =   this;

        app.shareManager().now.then(
        function( timestamp )
        {
            self.$set( 'now' , timestamp );
            // self.$set( 'now' , new Date( timestamp ) );
        });

        $( this.$els.file ).dropify({
            maxFileSize :  app.maxFileSize + 'M'
        });
    }
});
