<div>
    <nav class="" style="" v-cloak>
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" v-link-active><a v-link="{ name: 'create-quick' }">Quick</a></li>
            <li role="presentation" v-link-active><a v-link="{ name: 'create-file' }">File</a></li>
            <li role="presentation" v-link-active><a v-link="{ name: 'create-text' }">Text</a></li>
            <li role="presentation" v-link-active><a v-link="{ name: 'create-url' }">Url</a></li>
        </ul>
    </nav>

    <router-view></router-view>
</div>
