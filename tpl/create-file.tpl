<form class="" @submit.prevent="createFile">


    <!--<div class="form-group">
        <label for="create-file-file">File</label>
        <div class="input-group">
            <div class="btn-group input-group-btn">
                <input type="button" @click="selectFile" class="btn btn-primary" value="Select file" />
            </div>
            <input type="file" v-el:file multiple id="create-file-file" accept="*/*" class="form-control" />
            <!--<input type="file" multiple id="create-file-file" accept="text/*" class="form-control" />-->
        <!--</div>
    </div>
    -->

    <div class="form-group">
        <label for="create-file-file">File</label>
        <input type="file" v-el:file multiple id="create-file-file" accept="*/*" class="form-control" />
    </div>
    <div class="form-group">
        <div class="progress">
            <div class="progress-bar" v-el:progress role="progressbar" style="min-width: 2em; width: 0%;">
                0%
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="create-file-filename">Filename</label>
        <input v-model="filename" type="text" class="form-control" id="create-file-filename" placeholder="Filename (optional)" />
    </div>
    <div class="form-group">
        <label for="create-file-valid-views">Valid for views</label>
        <input v-model="validViews" type="number" class="form-control" id="create-file-valid-views" placeholder="Number of views" />
    </div>
    <div class="form-group">
        <label for="create-file-valid-until">Valid until</label>
        <input v-model="validUntil" type="text" class="form-control" id="create-file-valid-until" placeholder="{{ new Date(now) }}" />
    </div>

    <div class="form-group">
        <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-warning">Create</button>
        </div>
    </div>
</form>