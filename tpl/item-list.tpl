<table class="table table-condensed table-responsive table-hover">
    <thead>
        <tr>
            <th>url</th>
            <th>type</th>
            <th>created</th>
            <th>views</th>
            <th>delete</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(shareId,share) in shares" track-by="$index" v-cloak>
            <td>
                <a v-link="{ name: 'share' , params: { shareId : shareId } }">{{ shareId }}</a>
                <template v-if="share.type == 'file'">
                    ({{ cutString( share.filename , cutTo ) }})
                </template>
                <template v-if="share.type == 'text'">
                    ({{ cutString( share.text , cutTo ) }})
                </template>
                <template v-if="share.type == 'url'">
                    ({{ cutString( share.url , cutTo ) }})
                </template>
            </td>
            <td>{{ share.type }}</td>
            <td>
                <span title="{{ $momentFormatted( share.created ) }}">
                    {{ $momentFromNow( share.created ) }}
                </span>
            </td>
            <td>{{ share.views }}</td>
            <td>
                <a href="#" @click.prevent="removeShare(shareId)" class="btn btn-danger glyphicon glyphicon-trash"></a>
            </td>
        </tr>
    </tbody>
</table>