<form class="" @submit.prevent="login">
    <div class="form-group">
        <label for="login-email">Email</label>
        <input v-model="email" type="email" class="form-control" id="login-email" placeholder="Your email" />
    </div>
    <div class="form-group">
        <label for="login-password">Password</label>
        <input v-model="password" type="password" class="form-control" id="login-password" placeholder="Your password" />
    </div>

    <div class="form-group">
        <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-success">Login</button>
        </div>
    </div>
</form>