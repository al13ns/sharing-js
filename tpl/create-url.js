
var cmp = Vue.extend(
    {
        data        :   function()
        {
            return {
                url         :   null ,
                validViews  :   null ,
                validUntil  :   null ,
                now         :   null
            };
        },
        methods     :
        {
            createUrl  :   function()
            {
                var app = this.$route.router.app;
                var self = this;

                app.shareManager()
                .createUrl(
                    app.loggedUser().uid ,
                    self.url )
                .then(
                function( shareId )
                {
                    self.$route.router.go({ name : 'share' , params : { shareId : shareId } });
                });
            }
        },
        ready   :   function()
        {
            var manager =   this.$route.router.app.shareManager();
            var self    =   this;

            manager.now.then(
                function( timestamp )
                {
                    self.$set( 'now' , timestamp );
                });
        }
    });
