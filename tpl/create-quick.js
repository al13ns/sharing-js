
var cmp = Vue.extend(
{
    route    :  {
        canReuse : function(trans)
        {
            return false;
        }
    },
    data        :   function()
    {
        return {
            uploads :   []
        };
    },
    methods     :
    {

        base64toBlob    :   function(base64Data, contentType)
        {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        },

        remoteFileToBlob    :   function( url )
        {
            var self = this;

            return new Promise(
            function( resolve , reject )
            {
                var blob    =   null;
                var xhr     =   new XMLHttpRequest();
                xhr.open( "GET" , url );
                xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
                xhr.onload = function()
                {
                    blob = xhr.response;//xhr.response is now a blob object
                    resolve( blob );
                }
                xhr.send();
            });
        },

        getUploadCallback   :   function( i )
        {
            var self = this;

            return function( uploadTask )
            {
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                function(snapshot)
                {
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    progress    =   Math.round( progress );

                    self.$log('Upload #'+ i +' is ' + progress + '% done');

                    //update progress bar
                    $( '.progress-bar' ).eq( i )
                        .css( 'width' , progress + '%' )
                        .text( progress + '%' )
                    ;

                    switch (snapshot.state)
                    {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            self.$log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            //self.$log('Upload is running');
                            break;
                    }
                },
                function(error)
                {
                    switch (error.code) {
                        case 'storage/unauthorized':
                            // User doesn't have permission to access the object
                            break;

                        case 'storage/canceled':
                            // User canceled the upload
                            self.$log('Upload was cancelled');
                            break;

                        case 'storage/unknown':
                            // Unknown error occurred, inspect error.serverResponse
                            break;
                    }
                },
                function()
                {
                    // Upload completed successfully, now we can get the download URL
                    var downloadURL = uploadTask.snapshot.downloadURL;

                    self.$log( 'Upload #'+ i +' finished' );
                });
        };
        },

        handleDataTransfer  :   function( dataTransfer )
        {
            var app     =   this.$route.router.app;
            var manager =   app.shareManager();
            var self    =   this;

            console.log( dataTransfer );
            console.log( dataTransfer.types );
            console.log( dataTransfer.getData('image/png') );
            console.log( dataTransfer.getData('image') );
            console.log( dataTransfer.getData('text/plain') );
            console.log( dataTransfer.getData('text/html') );

            console.log( dataTransfer.files );

            // files
            // image data (prtscrn)
            // text
            // url

            return new Promise(
            function( resolve , reject )
            {
                if( dataTransfer.files.length > 0 )
                {
                    //upload files

                    var promises    =   [];
                    var uploads     =   [];

                    for( var i = 0 ; i < dataTransfer.files.length ; i++ )
                    {
                        console.log( dataTransfer.files[i] );
                        console.log( dataTransfer.files[i].name );

                        var file    =   dataTransfer.files[i];

                        if( (file.size/1024/1024) > app.maxFileSize )
                        {
                            self.$log( 'file size exceeded' , 'warning' );
                            self.$log( 'files discarded' , 'warning' );
                            break;
                        }

                        uploads.push(i);

                        promises.push(
                            manager.createFile(
                                app.loggedUser().uid ,
                                file ,
                                file.name ,
                                self.getUploadCallback(i) )
                        );
                    }

                    self.$set( 'uploads' , uploads );

                    resolve( Promise.all( promises ) );
                    //return Promise.all( promises );
                }
                else
                {
                    var data    =   dataTransfer.getData( 'text' );

                    if( data )
                    {
                        if( -1 !== data.indexOf( '://' ) )
                        {
                            //url
                            resolve( manager.createUrl( app.loggedUser().uid , data ) );
                        }
                        else
                        {
                            //text
                            resolve( manager.createText( app.loggedUser().uid , data ) );
                        }
                    }
                    else
                    {
                        //image - extract dataurl from <img> in .paste
                        console.log('image ?');

                        setTimeout(function()
                        {
                            // console.log( $( self.$els.paste ).find( 'img' ).attr( 'src' ) );

                            var data =   $( self.$els.paste ).find( 'img' ).attr( 'src' );

                            if( !data )
                            {
                                throw "not an image";
                            }

                            console.log('yes,image');

                            if( 0 === data.indexOf( 'data:' ) )
                            {
                                var type    =   data.substring( 5 , data.indexOf( ';' ) );
                                data        =   data.substring( data.indexOf( ',' ) + 1 );


                                //dataurl.substring( dataurl.indexOf( ';' ) );
                                // var blob    =   new Blob([ dataurl ], { type : type }); //data:image/png;
                                var blob    =   self.base64toBlob( data , type );
                                blob.name   =   self.$momentUtime() + '.' + type.substring( 6 );

                                console.log( blob );

                                if( (blob.size/1024/1024) > app.maxFileSize )
                                {
                                    self.$log( 'file size exceeded' , 'warning' );
                                    reject( 'file size exceeded' ); return;
                                }

                                self.uploads.push(0);

                                resolve(
                                    manager.createFile(
                                        app.loggedUser().uid ,
                                        blob ,
                                        blob.name ,
                                        self.getUploadCallback(0) )
                                );
                            }
                            else
                            {
                                //url
                                resolve( manager.createUrl( app.loggedUser().uid , data ) );
                            }
                        }
                        , 100 );
                    }
                }
            })
            .then( function()
            {
                self.$route.router.go({ name: 'shares' });
            });
        }
    },
    ready   :   function()
    {
        var self    =   this;

        $( self.$els.paste ).on( "dragover" , function(e)
        {
            $( self.$els.paste ).addClass( 'bg-info' );
        });

        $( self.$els.paste ).on( "dragleave" , function(e)
        {
            $( self.$els.paste ).removeClass( 'bg-info' );
        });

        $( self.$els.paste ).on( "drop" , function(e)
        {
            e.preventDefault();
            // e.stopPropagation();

            $( self.$els.paste ).removeClass( 'bg-info' );
            $( self.$els.paste ).addClass( 'bg-success' );

            console.log("Dropped!");

            self.handleDataTransfer( e.originalEvent.dataTransfer );
        });

        $( self.$els.paste ).on("paste", function(e){


            var pastedData = e.originalEvent.clipboardData.getData('text');

            console.log("Pasted!");
            console.log( pastedData );
            console.log( 'items' , e.originalEvent.clipboardData.items );

            $( self.$els.paste ).addClass( 'bg-success' );

            self.handleDataTransfer( e.originalEvent.clipboardData );
        } );

        $( self.$els.paste ).focus();
    }
});
