
var themeSwitcher   =   new Vue({
    el      :   '#theme-switcher' ,
    data    :
    {
        themes  :   [
            'none' , 'default' , 'cerulean' , 'cosmo' , 'cyborg' , 'darkly' , 'flatly' ,
            'journal' , 'lumen' , 'paper' , 'readable' , 'sandstone' , 'simplex' , 'slate' ,
            'spacelab' , 'superhero' , 'united' , 'yeti'
        ],
    },
    methods :   {
        changeTheme :   function()
        {
            var theme = $(this.$els.theme).val();

            if( 'none' == theme )
            {
                Cookies.remove( 'theme' );
            }
            else
            {
                Cookies.set( 'theme' , theme , { expires: 7 } );
            }

            this.setLinkCss( theme );
            //window.location.reload(true);
        },
        setLinkCss    :   function( theme )
        {
            $link   =   $('#theme');

            if( 'none' == theme )
            {
                $link.attr( 'href' , 'assets/bootstrap/css/bootstrap-theme.min.css' );
            }
            else
            {
                $link.attr( 'href' , 'assets/bootstrap/themes/'+ theme +'.min.css' );
            }
        }
    },
    ready   :   function()
    {
        var theme = Cookies.get( 'theme' ) || 'none';
        $(this.$els.theme).val( theme );
        this.setLinkCss( theme );
    }
});
