<div>
<template v-if="item">

    <table class="table table-condensed table-responsive table-hover">
        <thead>
            <tr>
                <th colspan="2">
                    <input type="text" v-el:url readonly value="{{ url }}" class="form-control input-lg" @click="clickUrl()" data-clipboard-target="#url" id="url" />
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>ID:</th>
                <td>{{ item.id }}</td>
            </tr>
            <tr>
                <th>created</th>
                <td>
                    <span>{{ $momentFromNow( item.created ) }}</span>
                    <span class="small">( {{ $momentFormatted( item.created ) }} )</span>
                </td>
            </tr>
            <tr>
                <th>type</th>
                <td>{{ item.type }}</td>
            </tr>
            <tr>
                <th>views</th>
                <td>{{ item.views }}</td>
            </tr>
        </tbody>

        <tbody v-if="item.type == 'file'">
            <tr>
                <th>filename</th>
                <td>{{ item.filename }}</td>
            </tr>
            <tr>
                <th>type</th>
                <td>{{ item.metadata.contentType }}</td>
            </tr>
            <tr>
                <th>md5</th>
                <td>{{ item.metadata.md5Hash }}</td>
            </tr>
            <tr>
                <th>size</th>
                <td>{{ item.metadata.size }}</td>
            </tr>
            <tr>
                <th>download</th>
                <td>
                    <a href="{{ item.url }}" target="_blank" class="btn btn-primary">Download<a/>
                </td>
            </tr>
            <tr v-if="isImage( item )">
                <td colspan="2"><img :src="item.url" alt="image" style="max-width: 100%;" /></td>
            </tr>
        </tbody>

        <tbody v-if="item.type == 'text'">
            <tr>
                <th>text</th>
                <td>{{ item.text }}</td>
            </tr>
            <tr>
                <th>type</th>
                <td>js|php|html|css</td>
            </tr>
            <tr>
                <th>download</th>
                <td>
                    <a href="#" @click.prevent="downloadText(item)" class="btn btn-primary">Download<a/>
                </td>
            </tr>
        </tbody>

        <tbody v-if="item.type == 'url'">
            <tr>
                <th>url</th>
                <td>{{ item.url }}</td>
            </tr>
            <tr>
                <th>follow</th>
                <td>
                    <a href="{{ item.url }}" class="btn btn-primary">Follow link<a/>
                </td>
            </tr>
        </tbody>

    </table>

</template>
</div>
