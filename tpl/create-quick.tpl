<div>
    <div class="panel panel-primary paste-cntnr">
        <div class="panel-heading">
            <h3 class="panel-title">Paste or drag & drop here</h3>
        </div>
        <div class="panel-body paste" contenteditable="true" v-el:paste>
            &nbsp;
        </div>
    </div>

    <div class="form-group" v-for="(index,upload) in uploads">
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="min-width: 2em; width: 0%;">
                0%
            </div>
        </div>
    </div>

</div>

