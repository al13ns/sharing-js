

var cmp = Vue.extend(
{
    data        :   function()
    {
        return {
            username    :   "" ,
            password    :   "" ,
            email       :   ""
        };
    },
    methods     :
    {
        login    :   function()
        {
            if( !this.email )
            {
                this.$log( 'email is empty' , 'danger' );
                return;
            }
            if( !this.password )
            {
                this.$log( 'password is empty' , 'danger' );
                return;
            }

            var self = this;

            firebase.auth()
            .signInWithEmailAndPassword( self.email , self.password )
            .then( function( user )
            {
                console.log( user );
                self.$log( 'successfully logged in' , 'success' );
                self.$route.router.go({ name : 'profile' });
            })
            .catch( function( err )
            {
                self.$log( err.message , 'danger' );
                throw err;
            });
        }
    },
    ready : function()
    {
        if( this.$route.router.app.loggedUser() )
            this.$route.router.go({ name : 'profile' });
    }
});