Vue.mixin({
    methods:
    {
        $log : function( text , style , ele )
        {
            var $ele = $( ele || '.log' );

            style   =   style || 'default';
            $ele.html( $ele.html() + '<span class="text-'+style+'">' + text + '</span>' + '<br />' );
        },
        $logAllErrors : function()
        {
            var self = this;

            window.addEventListener( "error" , function(e)
            {
                console.log(e.error);
                console.log(e.message);

                self.$log( e.error , 'danger' );
                return false;
            });
        }
    }
});

var cmp = Vue.extend(
{
    data        :   function()
    {
        return {
        };
    },
    methods     :
    {
        clearLog    :   function()
        {
            $(this.$el).parent().find('.log').empty();
        }
    }
});
