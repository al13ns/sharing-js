
var cmp = Vue.extend(
{
    route    :  {
        canReuse : function(trans)
        {
            return false;
        }
    },
    data        :   function()
    {
        return {

        };
    },
    methods     :
    {

    },
    ready   :   function()
    {
        var app     =   this.$route.router.app;
        var self    =   this;

        if( app.isCurrentRoute( 'create' ) )
            this.$route.router.go({ name : 'create-quick' });
    }
});
