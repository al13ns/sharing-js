
var ItemType =
{
    FILE    :   'file' ,
    TEXT    :   'text' ,
    URL     :   'url'
    //snippet,markdown
};

class Manager
{
    fromUrl( url )
    {
        //view/read share by it's url (id)
        //return Item instance
    }

    constructor( db , sb )
    {
        this.db         =   db;
        this.sb         =   sb;
    }

    get uploader()
    {
        return new Uploader( this.sb );
    }

    get newRecord()
    {
        return {
            created    :   firebase.database.ServerValue.TIMESTAMP ,
            views      :   0,
            validViews :   0,
            validUntil :   0,
        };
    }

    get now()
    {
        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            self.db.child( 'now' ).set( firebase.database.ServerValue.TIMESTAMP )
            .then(function()
            {
                self.db.child( 'now' ).once( 'value' ,
                function( data )
                {
                    resolve( data.val() );
                });
            });
        });
    }

    dbref( shareId )
    {
        return this.db.child( 'shares' ).child( shareId );
    }

    sbref( shareId , filename )
    {
        return this.sb.child( shareId ).child( filename );
    }

    /**
     *
     * @param string shareId
     * @returns {Promise.<Boolean>}
     */
    isItemStale( shareId )
    {
        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            self.now.then(
            function( now )
            {
                self.dbref( shareId ).once( 'value' ,
                function( data )
                {
                    data = data.val();

                    if( !data )
                    {
                        reject( "not found" ); return;
                    }

                    if( data.validViews && data.views >= data.validViews )
                    {
                        resolve( true );
                        return;
                    }

                    if( data.validUntil && now >= data.validUntil )
                    {
                        resolve( true );
                        return;
                    }

                    resolve( false );
                });
            });
        });
    }

    createItem( data , userId )
    {
        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            //merge new record template
            // save share
            var store   =   Object.assign( self.newRecord , data );
            store.user  =   userId;
            var dbRef   =   self.db.child( 'shares' ).push();
            console.log( dbRef.key );
            console.log( store);
            dbRef.set( store ).then(function()
            {
                resolve( dbRef.key );
            });
        })
        .then( function( shareId )
        {
            //save user share
            var store           =   {};
            store[shareId]    =   firebase.database.ServerValue.TIMESTAMP;
            self.db.child( 'users' ).child( userId ).child( 'shares' ).update( store );

            //add to viewed to not create first view
            self.setVisitedCookie( shareId );

            return Promise.resolve( shareId );
        });
    }

    setVisitedCookie( shareId )
    {
        var visited  =   this.getCookie( 'viewed' , [] );

        if( -1 === visited.indexOf( shareId ) )
        {
            visited.push( shareId );
            this.setCookie( 'viewed' , visited );
        }
    }

    /**
     *
     * resolves after file is created AND uploaded
     * uploadCallback handles upload progress
     *
     * @param File                  file
     * @param string                name
     * @param function(uploadTask)  uploadCallback
     * @returns {Promise}
     */
    createFile( userId , file , name , uploadCallback )
    {
        var self    =   this;

        return new Promise(
        function( resolve , reject )
        {
            self.createItem( { type : ItemType.FILE } , userId ).then(
            function( shareId )
            {
                var update = {
                    filename    :   name || file.name
                };
                self.dbref( shareId ).update( update );

                var uploadTask  =   self.uploader.uploadFile({
                    file : file ,
                    dir : shareId ,
                    name : name || file.name ,
                    success : function()
                    {
                        resolve( shareId );
                    }
                });

                if( uploadCallback )
                    uploadCallback( uploadTask );
            });
        });
    }

    createUrl( userId , url )
    {
        return this.createItem({
            type : ItemType.URL ,
            url : url
        }, userId );

        //return link to share?
    }

    createText( userId , text )
    {
        return this.createItem({
            type : ItemType.TEXT ,
            text : text
        } , userId );
        
        //create and store text file
        var blob    =   new Blob([ text ], {type : 'text/plain'});
        blob.name   =   'note.txt';

        return this.createFile( blob );
    }

    removeItem( shareId )
    {
        console.log( 'removing share ' + shareId );

        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            self.dbref( shareId ).once( 'value' ,
            function( share )
            {
                share = share.val();

                if( !share )
                {
                    reject( "not found" ); return;
                }

                //delete share
                self.dbref( shareId ).remove()
                .then(function()
                {
                    //delete user share
                    return self.db
                            .child( 'users' )
                            .child( share.user )
                            .child( 'shares' )
                            .child( shareId )
                            .remove();
                })
                .then(function()
                {
                    if( share.type === ItemType.FILE )
                    {
                        //delete file
                        return self.sbref( shareId , share.filename ).delete();
                    }
                })
                .then(function()
                {
                    resolve();
                });
            });
        });
    }

    setCookie( name , value , expires )
    {
        Cookies.set( name , value , { expires:  expires === undefined ? 7 : expires } );
    }

    getCookie( name , defaultValue )
    {
        return Cookies.getJSON( name ) || ( defaultValue === undefined ? null : defaultValue );
    }

    incrementItemViews( shareId )
    {
        var self = this;

        var viewed  =   self.getCookie( 'viewed' , [] );

        console.log( viewed );
        console.log(viewed.indexOf( shareId ) );
        
        if( -1 !== viewed.indexOf( shareId ) )
            return Promise.resolve();

        return new Promise(
        function( resolve , reject )
        {
            self.dbref( shareId ).once( 'value' ,
            function( data )
            {
                data = data.val();

                if( !data )
                {
                    reject( "not found" ); return;
                }

                self.dbref( shareId ).child( 'views' ).set( data.views + 1 );

                //set cookie
                viewed.push( shareId );
                self.setCookie( 'viewed' , viewed );

                resolve();
            });
        });
    }

    fetchUser( userId )
    {
        //copy auth users to db (create,profile update)
    }

    fetchUserItems( userId )
    {
        //fetch items from user
        //pagination - limit & offset -> limitToLast()

        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            self.db.child( 'shares' )
            .orderByChild( 'user' )
            .equalTo( userId )
            .limitToLast( 100 )
            .once( 'value' ,
            function( data )
            {
                console.log( 'USERITEMS' , data.val() );

                if( !data.val() )
                {
                    reject( "not found" ); return;
                }

                resolve( data.val() );
            });
        });

        /**
        db.child( 'play' )
            .orderByChild( 'users/'+ uid +'/uid' )
            .equalTo( uid )
            .once( 'value' , function( data )
            **/
    }

    /**
     *
     * @param string shareId
     * @returns {Promise.<Item>}
     */
    fetchItem( shareId )
    {
        var self = this;

        return new Promise(
        function( resolve , reject )
        {
            self.isItemStale( shareId )
            .catch( function(err)
            {
                reject( err );
            })
            .then( function( isStale )
            {
                if( isStale )
                {
                    //if stale, delete record
                    self.removeItem( shareId );

                    reject( "expired" ); return;
                }

                //fetch
                self.dbref( shareId ).once( 'value' ,
                function( data )
                {
                    data    =   data.val();

                    if( !data )
                    {
                        reject( "not found" ); return;
                    }

                    //full fetch
                    if( data.type === ItemType.FILE )
                    {
                        self.sbref( shareId , data.filename ).getMetadata()
                        .then(function(metadata)
                        {
                            data.metadata = metadata;
                            data.url      = metadata.downloadURLs[0];

                            resolve( new FileItem( shareId , data ) );
                        })
                        .catch(function(error)
                        {
                            reject( error ); return;
                        });
                    }
                    else if( data.type === ItemType.URL )
                    {
                        resolve( new UrlItem( shareId , data ) );
                    }
                    else if( data.type == ItemType.TEXT )
                    {
                        resolve( new TextItem( shareId , data ) );
                    }
                });
            });
        });
    }

};

class Uploader
{
    constructor( sb )
    {
        this.sb         =   sb;
    }

    /**
     *
     * @param string                file
     * @param string                dir
     * @param string                name
     * @param object                meta
     * @param function(snapshot)    progress
     * @param function(err)         error
     * @param function              success
     * @returns {firebase.storage.UploadTask*}
     */
    uploadFile( params )
    {
//            var path    =   dir + "/" + file.name;
        var path    =   params.dir || '';
        path       +=   path.lastIndexOf( '/' ) >= ( path.length - 1 ) ? '' : '/';
        path       +=   params.name || params.file.name;

        var metadata = params.meta ||
        {
            contentType: params.file.type
        };

        var uploadTask = this.sb.child( path ).put( params.file , metadata );

        uploadTask.on( 'state_changed' ,
        function( snapshot )
        {
            console.log( 'PROGRESS' );

            if( params.progress )
                params.progress( snapshot );
        } ,
        function( err )
        {
            console.log( 'ERROR' );

            if( params.error )
                params.error( err );
        } ,
        function()
        {
            console.log( 'SUCCESS' );

            if( params.success )
                params.success( uploadTask.snapshot );
        });

        return uploadTask;
    }

    uploadFiles( fileInput )
    {
        //TODO: use generator?

        for( var i = 0 ; i < fileInput.files.length ; i++ )
        {
            var file    =   fileInput.files[i];

            uploadFile( file );
            this.uploadFile();

            console.log( file.name );
            console.log( file.type );
            console.log( file.size );
        }
    }
};

class Item
{
    constructor( itemId , itemData )
    {
        this.id         =   itemId;
        this.created    =   itemData.created;
        this.type       =   itemData.type;
        this.user       =   itemData.user;
        this.views      =   itemData.views;
        this.validViews =   itemData.validViews || null;
        this.validUntil =   itemData.validUntil || null;
    }

    static createByType( itemId , itemData ) //TODO: not using anywhere, delete?
    {
        if( itemData.type === ItemType.FILE )
            return new FileItem( itemId , itemData );
        else if( itemData.type === ItemType.URL )
            return new UrlItem( itemId , itemData );
        else if( itemData.type === ItemType.TEXT )
            return new TextItem( itemId , itemData );

        throw new Error( 'invalid type' );
    }
};

class FileItem extends Item
{
    constructor( itemId , itemData )
    {
        super( itemId , itemData );
        this.filename   =   itemData.filename;
        this.metadata   =   itemData.metadata;
        this.url        =   itemData.url;
    }
};

class UrlItem extends Item
{
    constructor( itemId , itemData )
    {
        super( itemId , itemData );
        this.url        =   itemData.url;
    }
};

class TextItem extends Item
{
    constructor( itemId , itemData )
    {
        super( itemId , itemData );
        this.text        =   itemData.text;
    }
};

class User
{
    constructor( userData )
    {
        this.name   =   userData.name;
        this.id     =   userData.id;
    }

    get items()
    {
        //fetch items from fb, some limit/paging
    }

    getItems( offset , limit )
    {

    }
};

class GarbageCollector
{
    constructor( db , sb )
    {
        this.db         =   db;
        this.sb         =   sb;

        // orderByValue, startAt, endAt
    }

    purge()
    {
        this.cleanStaleViews();
        this.cleanStaleTime();
    }

    cleanStaleViews()
    {
        // delete all items with views >= validViews
    }

    cleanStaleTime()
    {
        // delete all items with now >= validUntil
    }
};

