
class FragmentLoader
{
    loadFragment( ele , uri )
    {
        return new Promise(
        function( resolve , reject )
        {
            try
            {
                $(ele).load( uri , function()
                {
                    resolve();
                });
            }
            catch(err)
            {
                reject(err);
            }
        });
    }

    loadScript( uri )
    {
        return new Promise(
        function( resolve , reject )
        {
            try
            {
                $.getScript( uri , function()
                {
                    resolve();
                });
            }
            catch(err)
            {
                reject(err);
            }
        });
    }
}

class TemplateLoader extends FragmentLoader
{
    constructor( tplUri )
    {
        super();

        tplUri = tplUri || 'tpl/';

        this.tplUri = tplUri;
    }

    loadTemplate( ele )
    {
        return this.loadFragment( ele , this.getTplUri( ele ) );
    }

    getTplUri( ele )
    {
        var name = $(ele).data( 'tpl-src' ) || $(ele).data('name');
        return this.tplUri + name + '.tpl';
    }
}

class ComponentLoader extends TemplateLoader
{
    constructor( tplUri , jsUri )
    {
        super( tplUri );

        jsUri = jsUri || this.tplUri;

        this.jsUri  =   jsUri;
    }

    loadComponent( ele )
    {
        var jsSrc   =   this.getJsUri( ele );
        var self    =   this;

        return new Promise(
        function( resolve , reject )
        {
            self.loadTemplate( ele )
            .then( function()
            {
                return self.loadScript( jsSrc );
            })
            .then( function()
            {
                Vue.component( $(ele).data('name') ,
                    cmp.extend({
                        template: ele
                    }) );
            })
            .then( function()
            {
                resolve();
            });
        });
    }

    loadComponents( eles )
    {
        var promises = [];

        for( var i in eles )
        {
            promises.push( this.loadComponent( eles[i] ) );
        }

        return Promise.all( promises );
    }

    mountComponent( ele , mountEle )
    {
        var self    =   this;

        return new Promise(
        function( resolve , reject )
        {
            self.loadComponent( ele ).then(
            function()
            {
                try
                {
                    var $mount  =   mountEle ? $(mountEle) : $(ele).parent();

                    if( !$mount.attr('id') )
                        throw new Error();

                    resolve(
                        new Vue({
                            el: '#' + $mount.attr('id')
                        })
                    );
                }
                catch(err)
                {
                    reject( "mount element must have an id attribute" );
                }
            });
        });
    }

    mountComponents( eles )
    {
        var promises = [];

        for( var i in eles )
        {
            promises.push( this.mountComponent( eles[i] ) );
        }

        return Promise.all( promises );
    }

    getJsUri( ele )
    {
        var name = $(ele).data( 'js-src' ) || $(ele).data('name');
        return this.jsUri + name + '.js';
    }
}

var VueLoader   =
{
    install :   function( Vue , options )
    {
        var tplUri  =   options && options['tplUri'] || null;
        var jsUri   =   options && options['jsUri'] || null;

        Vue.loader         =   new ComponentLoader( tplUri , jsUri );
        Vue.loadTemplate   =   function( ele )
        {
            return Vue.loader.loadTemplate( ele );
        };
        Vue.loadComponent   =   function( ele )
        {
            return Vue.loader.loadComponent( ele );
        };
        Vue.mountComponent   =   function( ele , mountEle )
        {
            return Vue.loader.mountComponent( ele , mountEle );
        };
        Vue.loadComponents   =   function( eles )
        {
            return Vue.loader.loadComponents( eles );
        };
        Vue.mountComponents   =   function( eles )
        {
            return Vue.loader.mountComponents( eles );
        };

        Vue.getComponent    =   function( name )
        {
            return Vue.options.components[name];
        }
    }
};