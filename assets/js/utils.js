
var Utils	=	
{
	Files	:	class Files
	{
		static loadFile( file , mode ) //mode - text, binary, dataurl, array
		{
			return new Promise(
			function( resolve , reject )
			{
				var reader  = new   FileReader();
				reader.onload = function(event)
				{
					// The file's text will be printed here
					console.log (event.target );
					console.log (event.target.result );
					resolve( event.target.result );
				};

				if( 'text' === mode )
				{
					reader.readAsText( file );
//        reader.readAsText( file , 'CP1250' );
				}
				else if( 'binary' === mode )
				{
					reader.readAsBinaryString( file );
				}
				else if( 'dataurl' === mode )
				{
					reader.readAsDataURL( file );
				}
				else if( 'array' === mode )
				{
					reader.readAsArrayBuffer( file );
				}
				else
				{
					reject( 'wrong mode - text|binary|dataurl|array' );
				}
			});
		}

		static loadTextFile( file )
		{
			// return Utils.Files.loadFile( file , 'text' )
			return this.loadFile( file , 'text' );
		}

		static loadDataUrlFile( file )
		{
			return this.loadFile( file , 'dataurl' );
		}

		static loadBinaryFile( file )
		{
			return this.loadFile( file , 'binary' );
		}
	}
};

/**
 <input type="file" id="pgp-message-file" accept="text/*" style="display: none;" onchange="console.log(this.files); loadFileContent( this.files[0] , $( '#pgp-message' ) );" />
 $('#pgp-message-file').click();
 */
